Exercice “Alerte crues du Clain”
================================

La mairie de Poitiers souhaite disposer d’un outil permettant d’informer rapidement les habitants des abords de la rivière “le Clain”.

Pour se faire, vous disposez de :

* **data.csv** : ensemble des dernières côtes effectuées à la station Poitiers [Pon Neuf]. Ce jeu de données comprend la date de prélèvement et la hauteur observée.

* **alert.csv** : liste des personnes inscrites au registre d’alerte. Ce jeu de données comprend l’adresse mail de la personne concernée ainsi que la hauteur à compter de laquelle cette personne doit être alertée.

* **utils.py** : module Pyton mettant à disposition plusieurs fonctions utiles. À savoir

    * *get_data()* : lecture de données csv
    * *post_data()* : écriture de données csv
    * *get_new_date()*: calcul de date


* **vigicrues.py** : module Pyton dans lequel doivent être codées l’ensemble de vos fonctions

* **simulation.py** : module Pyton permettant de simuler et tester le module vigicrues


A partir des données, ils vous est demandé de pouvoir :
_______________________________________________________

1. Afficher l’ensemble des mesures effectuées
2. Ajouter de nouvelles données à data.csv
3. Afficher les données d’une date donnée
4. Afficher les 3 dernières côtes présentes dans data.csv  (penser aux “slices” dans les listes)
5. Afficher une alerte si la hauteur du Clain dépasse un certain niveau. Cette alerte doit afficher un message contenant la hauteur limite de déclenchement de l’alerte + la hauteur actuelle du Clain + le mail de la personne à alerter
6. Ouvrir le site de vigicrues ("https://www.vigicrues.gouv.fr/niv3-station.php?CdEntVigiCru=12&CdStationHydro=L234161002&GrdSerie=H&ZoomInitial=3") dans un onglet du navigateur
7.  Pour ouvrir un site dans un navigateur, vous pouvez utiliser la fonction **webbrowser.open()** en important le packet **webbrowser**


Instructions supplémentaires :
______________________________
* Respecter les conventions de nommage (PEP8)
* Penser à ajouter des commentaires à vos fonctions (docstring)
* L’ensemble des fonctions et du code métier de votre application doit être contenu dans le module **vigicrues.py**
* Si le module **vigicrues.py** est exécuté directement, il doit afficher un titre et une description générale de l’application ainsi que les 3 derniers relevés de la station
* Les fichiers **data.csv** et **alert.csv** doivent avoir 1 saut de ligne maximum en fin de fichier pour fonctionner
* indiquer vos noms, prénoms, adresse mail dans __init__.py

* Le module **simulation.py** doit contenir le code du scénario suivant :
    * Afficher l’ensemble des mesures
    * Ajouter 20 côtes avec 1 heure d'intervalle chacune et une augmentation de 0.02m pour chaque côte
    * Les utilisateurs doivent être avertis dès que la hauteur de Clain est supérieure ou égale à leur propre niveau d’alerte
    * ouvrir le site vigicrues
* Un zip final doit être rendu contenant le paquet **“vigicrues”** avec l’ensemble des modules et fonctions

