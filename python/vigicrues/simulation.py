from python.vigicrues.vigicrues import *

"""
    Réalisation: Kévin PAULMIER
    Date: 12/04/2020

    Simulation du scénario suivant:

    a. Afficher l’ensemble des mesures
    b. Ajouter 20 côtes avec 1 heure d'intervalle chacune et une augmentation de
        0.02m pour chaque côte
    c. Les utilisateurs doivent être avertis dès que la hauteur de Clain est supérieure
        ou égale à leur propre niveau d’alerte
"""

# Afficher l'ensemble des mesures
afficher_toutes_mesures()

# Ajouter 20 côtes avec 1 heure d'intervalle chacune et une augmentation de 0.02m pour chaque côte
last_mesure = dernieres_mesures(1)
date = last_mesure[0][1]
niveau = float(last_mesure[0][2])

for mesure in range(0, 20):
    date = get_new_date(date)
    niveau = round(niveau + 0.02, 2)
    ajouter_mesure(niveau, date)

    # Les utilisateurs doivent être avertis dès que la hauteur de Clain est supérieure
    # ou égale à leur propre niveau d’alerte
    alerte_niveau(niveau)