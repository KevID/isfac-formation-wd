import csv
from datetime import datetime, timedelta


def get_data(csv_file='data.csv'):
    """
        Permet de lire et de recupérer les données d'un fichier csv avec 2 colonnes
        Retourne les données dans une liste de dictionnaire.
        Les dictionnaires sont composés de 3 colonnes.
        col 0 = numéro de la ligne
        col 1 = 1ere colonne du csv
        col 2 = 2eme colonne du csv

        :param csv_file: optionnel, nom du fichier csv à lire
        :return: retourne les données dans une liste de dictionnaire
    """
    data_list = []

    with open(csv_file, 'r') as csvfile:
        csv_reader = csv.reader(csvfile, delimiter=';')
        i = 0
        for row in csv_reader:
            if not i == 0:
                data = {}
                data[0] = i
                data[1] = row[0]
                data[2] = float(row[1])
                data_list.append(data)
            i += 1
            
    return data_list


def post_data(data, csv_file='data.csv'):
    """
        Permet d'ajouter des données à un fichier csv

        :param data: données à ajouter au fichier csv. les données "data" doivent être au format string
        :param csv_file: optionnel, nom du fichier csv à lire
    """

    file = open(csv_file, "a")
    file.write(data + '\n')
    file.close()


def get_new_date(current_date, nb_hours=1):
    """
        Permet de calculer une nouvelle date à partir d'une date donnée et en y ajoutant un nombre d'heure
        Par exemple : "2020-03-08 21:00" + 4heures = "2020-03-09 01:00"

        Plus d'exemple et d'explication sur la gesion des heures ici :
        https://www.programiz.com/python-programming/datetime/strftime

        :param current_date: date à partir de laquelle effectuer le calcul
        :param nb_hours: nombre d'heures à ajouter
        :return: retourne la nouvelle date calculée avec le nombre d'heure choisi en plus
    """

    date = datetime.strptime(current_date, '%Y-%m-%d %H:%M')
    new_date = date + timedelta(hours=nb_hours)
    new_date = format(new_date, '%Y-%m-%d %H:%M')

    return new_date

