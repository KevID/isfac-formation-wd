from python.vigicrues.utils import *

"""
    Réalisation: Kévin PAULMIER
    Date: 12/04/2020

    Exercice “Alerte crues du Clain”
"""


def new_line_data(date, niveau):
    """
        Permet de formater les données sous forme d'une chaine pour insérer dans le fichier csv

        :param date: date de la mesure
        ;param niveau: niveau de la mesure en m
        :return: retourne les données dans une chaine string
    """

    new_line = f"\"{date}\";{niveau}"

    return new_line


def afficher_toutes_mesures(csv_file='data.csv'):
    """
        Permet d'afficher toutes les mesures du fichier csv

        :param csv_file: optionnel, nom du fichier csv à lire
    """

    datas = get_data(csv_file)
    for data in datas:
        print(f"Le {data[1]} le niveau était à {data[2]} m \n")


def ajouter_mesure(niveau, date='null', csv_file='data.csv'):
    """
        Permet d'insérer une nouvelle mesure au fichier csv

        :param niveau: mesure du niveau de l'eau en m
        :param date: optionnel, date au format '%Y-%m-%d %H:%M'
        :param csv_file: optionnel, nom du fichier csv
    """

    niveau = float(niveau)

    if date == 'null':
        datas = get_data(csv_file)
        last_date = datas[-1][1]
        new_date = get_new_date(last_date)
    else:
        new_date = date

    if niveau > 0:
        post_data(new_line_data(new_date, niveau))
        print(f"{niveau} m pour le {new_date} a été enregistré avec succès.\n")
    else:
        print(f"Vous avez renseigné \"{niveau}\" mais cette valeur n'est pas correcte.\nAction annulée.\n")


def afficher_mesure(date, csv_file='data.csv'):
    """
        Permet d'afficher les données en fonction d'une date

        :param date: date au format '%Y-%m-%d %H:%M'
        :param csv_file: optionnel, nom du fichier csv
    """

    datas = get_data(csv_file)
    for data in datas:
        if date in data[1]:
            print(f"Le {date} le niveau était de {data[2]} m.\n")


def dernieres_mesures(nb_mesures=1, csv_file='data.csv'):
    """
        Permet de récupérer les x dernières mesures du fichier csv

        :param nb_mesure: nombre de mesures au format nombre entier
        :param csv_file: optionnel, nom du fichier csv
        ;return: retourne les x dernières mesures
    """

    nb_mesures = int(nb_mesures)
    datas = get_data(csv_file)
    last_mesures = datas[(0-nb_mesures):]

    return last_mesures


def alerte_niveau(niveau, csv_file='alert.csv'):
    """
        Permet d'alerter si le niveau dépasse un seuil défini dans le fichier csv

        :param niveau: mesure du niveau de l'eau en m
        :param csv_file: optionnel, nom du fichier csv
    """

    niveau = float(niveau)
    datas = get_data(csv_file)
    for data in datas:
        if niveau > data[2]:
            print(f"Alerte! le niveau de {niveau} m dépasse le seuil de {data[2]} m, veuillez alerter \"{data[1]}\".\n")


def accueil():
    """
        Permet d'afficher les informations d'accueil du programme
    """

    print("\n\n----------------------------------------------------------\n")
    print("Alerte crues du Clain, pour pratiquer le kayak sereinement !\n\n")
    print("Ce programme permet d'enregistrer les mesures du niveau du Clin\n"
          "et d'envoyer des alertes par email en cas de dépassement de seuils.\n\n")
    print("Les 3 derniers enregistrements:\n")
    for data in dernieres_mesures(3):
        afficher_mesure(data[1])


if __name__ == '__main__':
    accueil()
